import 'package:flutter/material.dart';
import 'package:go_to_movies/http_helper.dart';
import 'package:go_to_movies/pages/movie_detail.dart';

class ListMovies extends StatefulWidget {
  const ListMovies({Key? key}) : super(key: key);

  @override
  State<ListMovies> createState() => _ListMoviesState();
}

class _ListMoviesState extends State<ListMovies> {
  final String iconBase = 'https://image.tmdb.org/t/p/w92/';
  final String imageParDefaut =
      'https://images.freeimages.com/images/large-previews/5eb/movie-clapboard-1184339.jpg';

  Icon iconVisible = const Icon(Icons.search);
  Widget barreRecherche = const Text('Movies');

  late List _movies;
  late int _numberMovies = 0;
  late bool _isRevertSort = true;
  late HttpHelper helper;

  @override
  void initState() {
    super.initState();
    helper = HttpHelper();
    if (mounted) {
      initialiser();
    }
  }

  Future initialiser() async {
    List movies = await helper.receiveNewMovies();
    setState(() {
      _numberMovies = movies.length;
      _movies = movies;
    });
  }

  Future search(text) async {
    List movies = await helper.searchMovie(text);
    setState(() {
      _numberMovies = movies.length;
      _movies = movies;
    });
  }

  void sort(bool revert) {
    _movies.sort((a, b) {
      return a.compareTo(b);
    });
    if (revert) {
      _movies = _movies.reversed.toList();
      _isRevertSort = true;
    } else {
      _isRevertSort = false;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    NetworkImage image;
    return Scaffold(
        appBar: AppBar(
          title: barreRecherche,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.sort_rounded),
              onPressed: () {
                sort(!_isRevertSort);
              },
            ),
            IconButton(
              icon: iconVisible,
              onPressed: () {
                setState(() {
                  if (iconVisible.icon == Icons.search) {
                    iconVisible = const Icon(Icons.cancel);
                    barreRecherche = TextField(
                      textInputAction: TextInputAction.search,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                      onSubmitted: (String text) {
                        search(text);
                      },
                    );
                  } else {
                    setState(() {
                      iconVisible = const Icon(Icons.search);
                      barreRecherche = const Text('Movies');
                    });
                  }
                });
              },
            ),
          ],
        ),
        body: ListView.builder(
            itemCount: _numberMovies,
            itemBuilder: (context, i) {
              CircleAvatar(child: Image.network(imageParDefaut));
              if (_movies[i].getPosterUrl() == null) {
                image = NetworkImage(imageParDefaut);
              } else {
                image = NetworkImage(iconBase + _movies[i].getPosterUrl());
              }
              return Card(
                  margin: const EdgeInsets.all(10),
                  color: Colors.white,
                  elevation: 20,
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    CircleAvatar(backgroundImage: image),
                    ListTile(
                      onTap: () {
                        MaterialPageRoute route = MaterialPageRoute(
                            builder: (_) => MovieDetail(
                                  movie: _movies[i],
                                ));
                        Navigator.push(context, route);
                      },
                      title: Text(_movies[i].getTitle()),
                      subtitle: Text(_movies[i].getSubtitle()),
                    )
                  ]));
            }));
  }
}

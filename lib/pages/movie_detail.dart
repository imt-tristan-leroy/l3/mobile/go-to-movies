import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../modeles/movie.dart';

class MovieDetail extends StatelessWidget {
  const MovieDetail({Key? key, required this.movie}) : super(key: key);
  final Movie movie;

  @override
  Widget build(BuildContext context) {
    const String basePosterUrl = "https://image.tmdb.org/t/p/w500/";
    String path;
    if (movie.posterUrl != null) {
      path = basePosterUrl + (movie.posterUrl);
    } else {
      path =
          'https://images.freeimages.com/images/large-previews/5eb/movie-clapboard-1184339.jpg';
    }
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Films'),
      ),
      body: SingleChildScrollView(
          child: Center(
        child: Column(children: [
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Image.network(path, height: screen.height / 1.5),
          ),
          Text(movie.overview)
        ]),
      )),
    );
  }
}

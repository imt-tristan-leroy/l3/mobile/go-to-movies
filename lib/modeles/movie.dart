class Movie implements Comparable {
  late int id;
  late String title;
  late double voteAverage;
  late String releaseDate;
  late String overview;
  late String posterUrl;

  static const String idKey = 'id';
  static const String titleKey = 'title';
  static const String voteAverageKey = 'vote_average';
  static const String releaseDateKey = 'release_date';
  static const String overviewKey = 'overview';
  static const String posterUrlKey = 'poster_path';

  Movie(this.id, this.title, this.voteAverage, this.releaseDate, this.overview,
      this.posterUrl);

  Movie.fromJson(Map<String, dynamic> jsonString) {
    id = jsonString[idKey];
    title = jsonString[titleKey];
    voteAverage = jsonString[voteAverageKey] * 1.0;
    releaseDate = jsonString[releaseDateKey];
    overview = jsonString[overviewKey];
    posterUrl = jsonString[posterUrlKey];
  }

  String getTitle() {
    return title;
  }

  String getSubtitle() {
    return 'Release date : $releaseDate - Average Note : $voteAverage';
  }

  String getPosterUrl() {
    return posterUrl;
  }

  @override
  int compareTo(other) {
    if (other == null) {
      return 0;
    }
    if (voteAverage < other.voteAverage) {
      return 1;
    }
    if (voteAverage > other.voteAverage) {
      return -1;
    }
    if (voteAverage == other.voteAverage) {
      return 0;
    }
    return 0;
  }
}

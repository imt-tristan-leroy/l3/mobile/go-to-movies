import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

import 'modeles/movie.dart';

class HttpHelper {
  final String urlBase = dotenv.env['MOVIE_BASE_URL']!;
  final String urlCmd = '/upcoming?';
  final String urlKey = 'api_key=${dotenv.env['MOVIE_API_KEY']!}';
  final String urlLangage = '&language=fr-FR';
  final String urlRecherche =
      '${dotenv.env['MOVIE_BASE_URL']!}?api_key=${dotenv.env['MOVIE_API_KEY']!}&language=fr-FR&query=';

  Future<List> receiveNewMovies() async {
    http.Response result =
        await http.get(Uri.parse(urlBase + urlCmd + urlKey + urlLangage));

    if (result.statusCode == HttpStatus.ok) {
      final jsonString = json.decode(result.body);
      final moviesMap = jsonString['results'];
      List movies = moviesMap.map((i) => Movie.fromJson(i)).toList();
      return movies;
    } else {
      return [];
    }
  }

  Future<List> searchMovie(String titre) async {
    final String query = urlRecherche + titre;
    http.Response result = await http.get(Uri.parse(query));
    if (result.statusCode == HttpStatus.ok) {
      final jsonString = json.decode(result.body);
      final moviesMap = jsonString['results'];
      List films = moviesMap.map((i) => Movie.fromJson(i)).toList();
      return films;
    } else {
      return [];
    }
  }
}
